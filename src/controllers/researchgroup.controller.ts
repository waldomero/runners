import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  param,
  patch,
  post,
  put,
  requestBody,
} from '@loopback/rest';
import {ResearchGroup} from '../models';
import {ResearchgroupRepository} from '../repositories';
import {AddResearcher} from './schemas/add-researcher';
import {AddResearchLine} from './schemas/add-researchLine';
import {Code} from './schemas/code';
import {DeleteResearcher} from './schemas/delete-researcher';
import {DeleteResearchLine} from './schemas/delete-researchLine';
import {Name} from './schemas/name';

export class ResearchgroupController {
  constructor(
    @repository(ResearchgroupRepository)
    public researchgroupRepository: ResearchgroupRepository,
  ) {}

  @post('/researchgroups', {
    responses: {
      '200': {
        description: 'ResearchGroup model instance',
        content: {
          'application/json': {schema: getModelSchemaRef(ResearchGroup)},
        },
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ResearchGroup, {
            title: 'NewResearchGroup',
            exclude: ['_id'],
          }),
        },
      },
    })
    researchgroup: Omit<ResearchGroup, '_id'>,
  ): Promise<ResearchGroup> {
    return this.researchgroupRepository.create(researchgroup);
  }

  @get('/researchgroups/count', {
    responses: {
      '200': {
        description: 'ResearchGroup model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.where(ResearchGroup) where?: Where<ResearchGroup>,
  ): Promise<Count> {
    return this.researchgroupRepository.count(where);
  }

  @get('/researchgroups', {
    responses: {
      '200': {
        description: 'Array of ResearchGroup model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(ResearchGroup, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async find(
    @param.filter(ResearchGroup) filter?: Filter<ResearchGroup>,
  ): Promise<ResearchGroup[]> {
    return this.researchgroupRepository.find(filter);
  }

  @get('/researchgroups/{id}', {
    responses: {
      '200': {
        description: 'ResearchGroup model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(ResearchGroup, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.filter(ResearchGroup, {exclude: 'where'})
    filter?: FilterExcludingWhere<ResearchGroup>,
  ): Promise<ResearchGroup> {
    return this.researchgroupRepository.findById(id, filter);
  }

  @get('/researchgroups/code', {
    responses: {
      '200': {
        description: 'ResearchGroup model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(ResearchGroup, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findByCode(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Code, {
            title: 'Code',
          }),
        },
      },
    })
    codeObj: Code,
  ): Promise<ResearchGroup> {
    return this.findByCodeAsync(codeObj.code);
  }

  @get('/researchgroups/name', {
    responses: {
      '200': {
        description: 'ResearchGroup model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(ResearchGroup, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findByName(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Name, {
            title: 'Name',
          }),
        },
      },
    })
    nameObj: Name,
  ): Promise<ResearchGroup> {
    return this.findByNameAsync(nameObj.name);
  }

  @patch('/researchgroups/researcher', {
    responses: {
      '204': {
        description: 'Resarcher Added to ResearchGroup successfully',
      },
    },
  })
  async addResearcherByCode(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(AddResearcher, {partial: true}),
        },
      },
    })
    addResearcherObj: AddResearcher,
  ): Promise<void> {
    const researhGroup = await this.findByCodeAsync(addResearcherObj.code);
    // eslint-disable-next-line no-unused-expressions
    researhGroup.researchers?.push(addResearcherObj.researcher);

    await this.researchgroupRepository.updateById(
      researhGroup._id,
      researhGroup,
    );
  }

  @patch('/researchgroups/research-line', {
    responses: {
      '204': {
        description: 'Resarch Line Added to ResearchGroup successfully',
      },
    },
  })
  async addResearchLineByCode(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(AddResearchLine, {partial: true}),
        },
      },
    })
    addResearchLineObj: AddResearchLine,
  ): Promise<void> {
    const researhGroup = await this.findByCodeAsync(addResearchLineObj.code);
    // eslint-disable-next-line no-unused-expressions
    researhGroup.researchLines?.push(addResearchLineObj.researchLine);

    await this.researchgroupRepository.updateById(
      researhGroup._id,
      researhGroup,
    );
  }

  @put('/researchgroups/', {
    responses: {
      '204': {
        description: 'ResearchGroup PUT success',
      },
    },
  })
  async updateByCode(
    @requestBody() researchgroup: ResearchGroup,
  ): Promise<void> {
    const researhGroup = await this.findByCodeAsync(researchgroup.code);
    researhGroup.name = researchgroup.name;
    researhGroup.status = researchgroup.status;
    researhGroup.description = researchgroup.description;

    await this.researchgroupRepository.updateById(
      researhGroup._id,
      researhGroup,
    );
  }

  @del('/researchgroups', {
    responses: {
      '204': {
        description: 'ResearchGroup Inactived successfully',
      },
    },
  })
  async inactivateByCode(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Code, {
            title: 'Code',
          }),
        },
      },
    })
    codeObj: Code,
  ): Promise<void> {
    const researhGroup = await this.findByCodeAsync(codeObj.code);
    researhGroup.status = 'Inactive';
    await this.researchgroupRepository.updateById(
      researhGroup._id,
      researhGroup,
    );
  }

  @del('/researchgroups/research-line', {
    responses: {
      '204': {
        description: 'ResearchLine detached successfully',
      },
    },
  })
  async removeResearchLineByCodeId(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(DeleteResearchLine, {
            title: 'DeleteResearchLine',
          }),
        },
      },
    })
    deleteResearchLineObj: DeleteResearchLine,
  ): Promise<void> {
    const researhGroup = await this.findByCodeAsync(deleteResearchLineObj.code);

    if (researhGroup.researchLines != undefined) {
      researhGroup.researchLines = researhGroup.researchLines.filter(
        rl => rl.id !== deleteResearchLineObj.idResearchLine,
      );
    }

    await this.researchgroupRepository.updateById(
      researhGroup._id,
      researhGroup,
    );
  }

  @del('/researchgroups/researcher', {
    responses: {
      '204': {
        description: 'Researcher detached successfully',
      },
    },
  })
  async removeResearcherByCodeId(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(DeleteResearcher, {
            title: 'DeleteResearcher',
          }),
        },
      },
    })
    deleteResearcherObj: DeleteResearcher,
  ): Promise<void> {
    const researhGroup = await this.findByCodeAsync(deleteResearcherObj.code);

    if (researhGroup.researchers != undefined) {
      researhGroup.researchers = researhGroup.researchers.filter(
        r => r.id !== deleteResearcherObj.idResearcher,
      );
    }

    await this.researchgroupRepository.updateById(
      researhGroup._id,
      researhGroup,
    );
  }

  async findByCodeAsync(codeIn: string): Promise<ResearchGroup> {
    const researhGroup = await this.researchgroupRepository.findOne({
      where: {code: codeIn},
    });

    if (researhGroup == null) {
      // eslint-disable-next-line no-throw-literal
      throw {
        statusCode: 404,
        name: 'Error',
        message: 'ResearchGroup not found with code: ' + codeIn,
        code: 'ENTITY_NOT_FOUND',
      };
    }
    return researhGroup;
  }
  async findByNameAsync(nameIn: string): Promise<ResearchGroup> {
    const researhGroup = await this.researchgroupRepository.findOne({
      where: {name: nameIn},
    });

    if (researhGroup == null) {
      // eslint-disable-next-line no-throw-literal
      throw {
        statusCode: 404,
        name: 'Error',
        message: 'ResearchGroup not found with name: ' + nameIn,
        code: 'ENTITY_NOT_FOUND',
      };
    }
    return researhGroup;
  }
}
