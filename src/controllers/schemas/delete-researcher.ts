import {Entity, model, property} from '@loopback/repository';

@model()
export class DeleteResearcher extends Entity {
  @property({
    type: 'string',
    required: true,
  })
  code: string;

  @property({
    type: 'number',
    required: true,
  })
  idResearcher: number;

  constructor(data?: Partial<DeleteResearcher>) {
    super(data);
  }
}

export type DeleteResearcherType = DeleteResearcher;
