import {Entity, model, property} from '@loopback/repository';

@model()
export class Code extends Entity {
  @property({
    type: 'string',
    required: true,
  })
  code: string;

  constructor(data?: Partial<Code>) {
    super(data);
  }
}

export type CodeType = Code;
