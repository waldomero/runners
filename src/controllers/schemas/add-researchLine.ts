import {Entity, model, property} from '@loopback/repository';
import {ResearchLine} from '../../models';

@model()
export class AddResearchLine extends Entity {
  @property({
    type: 'string',
    required: true,
  })
  code: string;

  @property({
    required: true,
  })
  researchLine: ResearchLine;

  constructor(data?: Partial<AddResearchLine>) {
    super(data);
  }
}

export type AddResearchLineType = AddResearchLine;
