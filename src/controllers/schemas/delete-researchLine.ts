import {Entity, model, property} from '@loopback/repository';

@model()
export class DeleteResearchLine extends Entity {
  @property({
    type: 'string',
    required: true,
  })
  code: string;

  @property({
    type: 'string',
    required: true,
  })
  idResearchLine: string;

  constructor(data?: Partial<DeleteResearchLine>) {
    super(data);
  }
}

export type DeleteResearchLineType = DeleteResearchLine;
