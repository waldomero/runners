import {Entity, model, property} from '@loopback/repository';

@model()
export class Name extends Entity {
  @property({
    type: 'string',
    required: true,
  })
  name: string;

  constructor(data?: Partial<Name>) {
    super(data);
  }
}

export type NameType = Name;
