import {Entity, model, property} from '@loopback/repository';
import {Researcher} from '../../models';

@model()
export class AddResearcher extends Entity {
  @property({
    type: 'string',
    required: true,
  })
  code: string;

  @property({
    required: true,
  })
  researcher: Researcher;

  constructor(data?: Partial<AddResearcher>) {
    super(data);
  }
}

export type AddResearcherType = AddResearcher;
