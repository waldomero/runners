import {Entity, model, property} from '@loopback/repository';
import {ResearchLine} from './research-line.model';
import {Researcher} from './researcher.model';

enum Status {
  Active = 'Active',
  Inactive = 'Inactive',
}

@model()
export class ResearchGroup extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: true,
  })
  _id?: string;

  @property({
    type: 'string',
    required: true,
    index: {
      unique: true,
    },
    unique: true,
  })
  code: string;

  @property({
    type: 'string',
    required: true,
  })
  name: string;

  @property({
    type: 'string',
    required: true,
    jsonSchema: {
      enum: Object.values(Status),
    },
  })
  status: string;

  @property({
    type: 'date',
    required: true,
  })
  creationDate: string;

  @property({
    type: 'string',
    required: true,
  })
  description: string;

  @property.array(ResearchLine)
  researchLines?: ResearchLine[];

  @property.array(Researcher)
  researchers?: Researcher[];

  constructor(data?: Partial<ResearchGroup>) {
    super(data);
  }
}

export interface ResearchgroupRelations {
  // describe navigational properties here
}

export type ResearchgroupWithRelations = ResearchGroup & ResearchgroupRelations;
