import {Model, model, property} from '@loopback/repository';

@model()
export class Researcher extends Model {
  @property({
    type: 'number',
    id: true,
    generated: false,
    required: true,
  })
  id: number;

  @property({
    type: 'string',
    required: true,
  })
  name: string;

  @property({
    type: 'date',
    required: true,
  })
  enrollmentDate: string;

  @property({
    type: 'boolean',
    default: false,
  })
  director?: boolean;

  constructor(data?: Partial<Researcher>) {
    super(data);
  }
}

export interface ResearcherRelations {
  // describe navigational properties here
}

export type ResearcherWithRelations = Researcher & ResearcherRelations;
