import {Model, model, property} from '@loopback/repository';

@model()
export class ResearchLine extends Model {
  @property({
    type: 'string',
    id: true,
    generated: false,
    required: true,
  })
  id: string;

  @property({
    type: 'string',
    required: true,
  })
  name: string;

  @property({
    type: 'date',
    required: true,
    default: Date.now,
  })
  enrollmentDate: string;

  constructor(data?: Partial<ResearchLine>) {
    super(data);
  }
}

export interface ResearchLineRelations {
  // describe navigational properties here
}

export type ResearchLineWithRelations = ResearchLine & ResearchLineRelations;
