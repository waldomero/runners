import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {MongoDsDataSource} from '../datasources';
import {ResearchGroup, ResearchgroupRelations} from '../models';

export class ResearchgroupRepository extends DefaultCrudRepository<
  ResearchGroup,
  typeof ResearchGroup.prototype._id,
  ResearchgroupRelations
> {
  constructor(@inject('datasources.mongoDS') dataSource: MongoDsDataSource) {
    super(ResearchGroup, dataSource);
  }
}
